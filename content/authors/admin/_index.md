---
# Display name
name: Aryeh Stock

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Pathology Resident

# Organizations/Affiliations
organizations:
- name: Icahn School of Medicine at Mount Sinai
  url: "www.mountsinai.org"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include pathology informatics and programmable matter.

interests:
- Artificial Intelligence
- Computational Linguistics
- Information Retrieval

education:
  courses:
  - course: AP/CP Pathology Residency
    institution: Icahn School of Medicine at Mount Sinai
    year: 2022  
  - course: MD
    institution: Icahn School of Medicine at Mount Sinai
    year: 2018
  - course: Bachelor of Arts in Biology
    institution: Yeshiva University
    year: 2012

  

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:aryeh.stock@mountsinai.org'  # For a direct email link, use "mailto:test@example.org".
# - icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/aryehstock
# - icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://github.com/aryehstock
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "stockari@gmail.com"
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

Aryeh Stock is a pathology resident and House Staff at the Icahn School of Medicine at Mount Sinai. His research interests include pathology informatics, mobile computing and programmable matter. He leads the PRPL_code group, a pathology resident programming laboratory which deploys collaboratively developed pathology projects.


